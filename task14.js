const themeButton = document.getElementById('theme-button');
const body = document.body;
let currentTheme = localStorage.getItem('theme') || 'light';

const applyTheme = () => {
  if (currentTheme === 'light') {
    body.style.backgroundColor = '#fff';
    themeButton.style.backgroundColor = '#000';
    themeButton.style.color = '#fff';
    
  } else {
    body.style.backgroundColor = '#7777';
    themeButton.style.backgroundColor = '#fff';
    themeButton.style.color = '#000';
    
  }
}

themeButton.addEventListener('click', () => {
  currentTheme = currentTheme === 'light' ? 'dark' : 'light';
  localStorage.setItem('theme', currentTheme);
  applyTheme();
});

applyTheme();
